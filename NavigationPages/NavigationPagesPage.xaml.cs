﻿using System;
using System.Diagnostics;
using Xamarin.Forms;

namespace NavigationPages
{
    public partial class NavigationPagesPage : ContentPage
    {
        public NavigationPagesPage()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(NavigationPages)}: ctor");
            InitializeComponent();

          
            NavigationPage.SetHasNavigationBar(this, false);
        }

        async void GoToPage1Clicked(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(GoToPage1Clicked)}");
            bool userRespond = await DisplayAlert("You will be viewing page 1.",
                                               "Are you sure you want to proceed?",
                                               "Yes. I want to proceed.",
                                                "No! Not interested.");

            if (userRespond == true)
            {
                await Navigation.PushAsync(new ThePage1());
            }

            //Navigation.PushAsync(new ThePage1());
        }

        async void GoToPage2Clicked(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(GoToPage2Clicked)}");

            string answered = await DisplayActionSheet("Guess what this image is this?",
                                                       "Cancel",
                                                      "NULL",
                                                      "Fish",
                                                       "Aero Plane");

            Debug.WriteLine($"User picked: {answered}");

            if(answered.Equals("Fish", StringComparison.OrdinalIgnoreCase))
            {
                await Navigation.PushAsync(new ThePage2());
            }

        }

        async void GoToPage3Clicked(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(GoToPage3Clicked)}");

            bool Response = await DisplayAlert("WARNING! You will be viewing conspiricy theory!!!.",
                                               "Are you sure you want to view this image?",
                                              "Yes I would like to.",
                                              "No thanks. I am good.");

            if (Response == true)
            {
                await Navigation.PushAsync(new ThePage3());
            }

        }
    }
}
