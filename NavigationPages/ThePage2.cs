﻿using System;

using Xamarin.Forms;

namespace NavigationPages
{
    public class ThePage2 : ContentPage
    {
        public ThePage2()
        {
            Content = new StackLayout
            {
                Children = {
                    new Label { Text = "Hello ContentPage" }
                }
            };
        }
    }
}

