﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Xamarin.Forms;

namespace NavigationPages
{
    public partial class ThePage1 : ContentPage
    {
        public ThePage1()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(ThePage1)}: ctor");
            InitializeComponent();
        }
    }
}
