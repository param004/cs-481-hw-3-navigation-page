﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Xamarin.Forms;

namespace NavigationPages
{
    public partial class ThePage2 : ContentPage
    {
        public ThePage2()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(ThePage2)}: ctor");
            InitializeComponent();
        }
    }
}
