﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Xamarin.Forms;

namespace NavigationPages
{
    public partial class ThePage3 : ContentPage
    {
        public ThePage3()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(ThePage3)}: ctor");
            InitializeComponent();
        }
    }
}
